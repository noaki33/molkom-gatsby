import React from 'react'
import styled from 'styled-components'

import Layout from '../components/layout'
import JobComponent from '../components/career-components/jobs'

const Wrapper = styled.div``
export default () => (
  <Layout>
    <Wrapper>
      <JobComponent /> 
    </Wrapper>
  </Layout>
)
