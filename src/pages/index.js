import React from 'react'
import styled from 'styled-components'

import Layout from '../components/layout'
// import SEO from '../components/seo'

import ImageComponent from '../components/index-components/image-component';
import AboutComponent from '../components/index-components/about';

const Wrapper = styled.div`
	display: flex;
	flex-direction: column;
	width: 100%;
`;

export default () => (
	<Layout>
		<Wrapper>
			<ImageComponent />
			<AboutComponent />
		</Wrapper>
	</Layout>
);

