import React from 'react'
import styled from 'styled-components'

import Layout from '../components/layout'
import Container from '../components/container'
import ContactComponent from '../components/contact-components/contact-component'

const Wrapper = styled(Container)`
    margin: 0 auto;
    flex-direction: column;
    justify-content:center;
`

const Text = styled.p`
width: 100%;
text-align: center;
`

const Title = styled.h2`
text-align:center;
`

export default() => (
    <Layout>
        <ContactComponent></ContactComponent>
        {/* <Wrapper>
            <Title>Dane firmy i kontakt</Title>
            <Text>MOLKOM</Text>
<Text>Paweł Bartczak</Text>
<Text>ul. Poznańska 46/1</Text>
<Text>62-800 Kalisz</Text>
<Text>NIP: 668-175-52-56 </Text>
<Text>e-mail: kontakt@molkom.pl </Text>

<Text>Usługi programistyczne, strony www:</Text>
<Text>Paweł Bartczak</Text>
<Text>tel. 692 931 521 </Text>

<Text>Obsługa informatyczna:</Text>
<Text>Michał Kujawa</Text>
<Text>tel. 604 629 583</Text>
        </Wrapper> */}
    </Layout>
)