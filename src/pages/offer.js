import React from 'react'
import styled from 'styled-components'

import Layout from '../components/layout'
import SingleOfferComponent from '../components/offer-components/single-offer-component'

import { Colors } from '../components/colors'
import { StaticQuery } from 'gatsby'

import devices from '../images/icons/devices.svg'
import chip from '../images/icons/chip.svg'
import laptop from '../images/icons/tablet.svg'

const Offer = () => (
  <StaticQuery
    query={graphql`
      query OfferQuery {
        allDataJson {
          edges {
            node {
              id
              title
              description
              image
              type
            }
          }
        }
      }
    `}
    render={data => (
      <Layout>
          {getOfferComponents(data)}
      </Layout>
    )}
  />
)

function getOfferComponents(data) {
  const images = [devices, chip, laptop];
  const components = [];
  let count = 0;
  data.allDataJson.edges.forEach(element => {
    if (element.node.type !== 'offerText') 
    return;
    components.push(
      <SingleOfferComponent
        key={element.node.id} 
        props={{
          title: element.node.title,
          description: element.node.description,
          image: images[count],
          odd: ++count % 2 ? true : false,
        }}
      />
    )
  })
  return components
}

export default Offer
