import React from 'react'
import styled from 'styled-components'
import { Link } from 'gatsby'

import { Media } from '../media'
import { Colors } from '../colors'

const NavItem = styled.li`
  font-size: 1rem;
  line-height: 1.3rem;
  text-align: center;
  margin: 0;
  height: 50px;
  line-height: 50px;
  transition: 0.2s border-bottom;

  @media screen and (min-width: ${Media.tablet}) {
    height: 60px;
    line-height: 60px;

  }
`

const StyledLink = styled(Link)`
  text-decoration: none;
  color: ${Colors.mainColor};
  text-transform: uppercase;
  display:block;
  padding: 0 20px;
  transition: 0.2s border-bottom;
  box-sizing: border-box;
  height: 100%;

  &:hover{
      text-decoration: none;
      border-bottom: 4px solid ${Colors.mainColor};
  }
`
const NavigationItem = ({ props }) => (
  <NavItem>
    <StyledLink to={props.link} activeStyle={{borderBottom: `4px solid ${Colors.mainColor}`}}>{props.text}</StyledLink>
  </NavItem>
)

export default NavigationItem
