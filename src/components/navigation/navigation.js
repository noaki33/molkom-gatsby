import React from 'react'
import styled from 'styled-components'

import { Media } from '../media'
import { Colors } from '../colors'
import NavigationItem from './navigation-item'
import Container from '../container'

import menu from '../../images/icons/menu.svg'


const Nav = styled.nav`
  width: 100%;

  @media screen and (min-width: ${Media.tablet}) {
    display: flex;
    justify-content: space-between;
    padding-bottom: 0;
    height: 60px;
    align-items: center;
  }
`

const Wrapper = styled(Container)`
  position: fixed;
  left: 0;
  top: 0;
  right: 0;
  line-height: 60px;
  background-color: #fff;
  position: relative;
  margin: 0 auto;
`

const NavigationList = styled.ul`
  list-style-type: none;
  display: ${props => (props.isOpen ? 'block' : 'none')};
  margin: 0;
  width: 100%;

  @media screen and (min-width: ${Media.tablet}) {
    display: flex;
    margin-right: 30px;
    flex-direction: row;
    justify-content: flex-end;
  }
`

const Brand = styled.a`
  color: ${Colors.mainColor};
  text-decoration: none;
  display: inline-block;
  text-transform: uppercase;
  &:hover{
    text-decoration:none;
  }
`

const ToggleButton = styled.span`
  position: absolute;
  top: 10px;
  right: 20px;
  cursor: pointer;
  font-size: 24px;
  background: url(${menu});
  background-repeat: no-repeat;
  background-size: contain;
  height: 40px;
  width: 40px;
  @media screen and (min-width: ${Media.tablet}) {
    display: none;
  }
`

class Navigation extends React.Component {
  constructor() {
    super();
    this.state = {
      isOpen: false,
    };
    this.toggleMenu = this.toggleMenu.bind(this);
  }

  toggleMenu() {
    this.setState(state => ({
      isOpen: !state.isOpen,
    }));
  }

  render() {
    const { isOpen } = this.state;
    return (
      <Wrapper>
        <ToggleButton onClick={this.toggleMenu}/>
        <Brand href='/molkom'>molkom</Brand>
        <Nav role="navigation">
          <NavigationList isOpen={isOpen}>
            <NavigationItem props={{ link: '/offer', text: 'oferta' }} />
            {/* <NavigationItem props={{ link: '/clients', text: 'Our clients' }} /> */}
            <NavigationItem props={{ link: '/contact', text: 'kontakt' }} />
            <NavigationItem props={{ link: '/career', text: 'kariera' }} />
          </NavigationList>
        </Nav>
      </Wrapper>
    );
  }
}

export default Navigation
