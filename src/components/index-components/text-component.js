import React from 'react'
import styled from 'styled-components'

import { Media } from '../media'
import { Colors } from '../colors'

const Wrapper = styled.div`
  box-sizing: border-box;
  width: 50%;
  padding: 0 20px;
  @media screen and (max-width: ${Media.tablet}) {
    width:100%;
  }
`

const Text = styled.p`
  color: ${Colors.mainColor};
`

const Title = styled.h2`
  color: ${Colors.mainColor};
`

export default ({props}) => (
  <Wrapper>
    <Title>{props.title}</Title>
    <Text>
      {props.description}
    </Text>
  </Wrapper>
)
