import React from 'react'
import styled from 'styled-components'
import { StaticQuery, graphql } from 'gatsby'

import { Media } from '../media'

import Container from '../container'
import AboutText from './text-component'

const Wrapper = styled(Container)`
  min-height: 450px;
  /* margin-top: -60px; */
  z-index: 2;
  position: relative;
  margin: 50px auto 0 auto;
  display: flex;
  flex-direction: row;
  @media screen and (max-width: ${Media.tablet}) {
    flex-direction: column;
  }
`

const About = () => (
  <StaticQuery
    query={graphql`
      query AboutQuery {
        allDataJson {
          edges {
            node {
              id
              title
              description
              type
            }
          }
        }
      }
    `}
    render={data => <Wrapper>{getAboutComponents(data)}</Wrapper>}
  />
)

function getAboutComponents(data) {
  const components = [];
  data.allDataJson.edges.forEach(element => {
    if(element.node.type !== 'about')
      return;
    components.push(
      <AboutText
        key={element.node.id}
        props={{
          title: element.node.title,
          description: element.node.description,
        }}
      />
    )
  })
  return components
}

export default About
