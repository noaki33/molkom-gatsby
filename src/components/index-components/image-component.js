import React from 'react'
import styled from 'styled-components'
import bg from '../../images/bg.jpg'
import { Colors } from '../colors';

const Wrapper = styled.div`
  min-height: 100vh;
  width: 100%;
	position: relative;
`

const Container = styled.div`
  min-height: 100vh;
  width: 100%;
  background: url(${bg});
  background-size: cover;
	background-repeat: no-repeat;
	background-attachment: fixed;
	background-position: center;
  display: flex;
  align-items: center;
	justify-content: center;
`

const TitleContainer = styled.div`
	z-index: 1;
`

const Title = styled.p`
	color: #fff;
	text-transform: uppercase;
	font-size: 2rem;
	text-align: center;
`

const Overlay = styled.div`
	position: absolute;
	height: 100%;
	width: 100%;
	top: 0;
	background-color: #000;
	opacity: 0.8;
`

const Span = styled.span`
	color: ${Colors.accentColor};
`

export default () => (
  <Wrapper>
    <Container>
		 <TitleContainer>
			 <Title>profesjonalne rozwiązania <Span>it</Span></Title>
		 </TitleContainer>
	 </Container>
	 <Overlay/>
  </Wrapper>
)