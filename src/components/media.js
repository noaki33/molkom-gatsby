export const Media = {
    mobile: '320px',
    tablet: '768px',
    laptop: '992px',
    desktop: '1440px',
  };