import React from 'react'
import styled from 'styled-components'

import { Colors } from '../colors'
import { Media } from '../media'
// import fontawesome from '@fortawesome/fontawesome'
import { faMapMarkerAlt } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const Wrapper = styled.div`
  margin: 20px;
  background-color: #fff;
  display: flex;
  flex-direction: column;
  position: relative;
  box-shadow: 0 10px 10px rgba(174,167,159,.3);
  border-radius: 10px;
`

const Container = styled.h3`
  line-height: 30px;
  text-align: center;
  margin: 0 10px 0 10px;
`
const Button = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  border: none;
  background-color: #fff;
  padding-bottom: 15px;
`

const JobDetails = styled.div`
  width: 100%;
  padding: 10px;
  @media screen and (min-width: ${Media.tablet}) {
    width: 50%;
  }
`

const Content = styled.div`
  overflow: hidden;
  display: flex;
  flex-direction: column;
  color:#fff;
  background-color: ${Colors.mainColor};
  border-bottom-left-radius: 10px;
  border-bottom-right-radius: 10px;
  @media screen and (min-width: ${Media.tablet}) {
    flex-direction: row;
  }
`
const List = styled.ul`
  list-style: disc;
`

const ListItem = styled.li`
    line-height: 1.1rem;
    /* color: ${Colors.mainColor}; */
`

const Icon = styled(FontAwesomeIcon)`
  margin: 0 6px;
  color: ${Colors.accentColor};
`

const HR = styled.hr`
  width: 80%;
  margin: 0 auto;
  background-image: linear-gradient(to right, #fff, ${Colors.mainColor}, #fff);
  height: 3px;
  opacity: 0.7;
`

function listArray(data) {
  const components = []
  data.forEach(element => {
    components.push(<ListItem key={element}>{element}</ListItem>)
  })
  return components
}

export default ({ props }) => (
  <Wrapper>
    <Button>
      <Container>{props.title}</Container>
      <Container>
        <Icon icon={faMapMarkerAlt} />
        {props.place}
      </Container>
    </Button>
    <Content>
      <JobDetails>
        <h4>Obowiązki:</h4>
        <List>{listArray(props.responsibilities)}</List>
      </JobDetails>
      <JobDetails>
        <h4>Wymagania:</h4>
        <List>{listArray(props.requirements)}</List>
      </JobDetails>
    </Content>
  </Wrapper>
)
