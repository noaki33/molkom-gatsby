import React from 'react'
import styled from 'styled-components'
import { StaticQuery, graphql } from 'gatsby'

import Container from '../container'
import SingleJob from '../career-components/single-job'

import {Colors} from '../colors'

const Wrapper = styled(Container)`
  margin: 0 auto;
  display: flex;
  flex-direction: column;
  justify-content: center;
  margin-bottom:100px;
`

const Text = styled.h1`
  text-transform: uppercase;
  text-align: center;
  border: none;
  color: ${Colors.mainColor};
`

const JobsContainer = styled.div`
  display:flex;
  flex-direction: column;
  flex-wrap: wrap;
  margin-bottom: 50px;
`

const MailText = styled.div`
text-align:center;
margin: 10px 0;
`

const MailPostfix = styled.div`
font-style: italic;
font-size: 13px;
text-align:center;
`

const Mail = styled.a`
margin: 0 auto;
line-height:50px;
display:block;
height:50px;
background: ${Colors.mainColor};
width: 150px;
text-decoration: none;
color:#fff;
/* text-transform: uppercase; */
text-align:center;
&:hover{
  text-decoration:none;
}
`

function getJobComponents(data) {
  const components = [];
  data.allDataJson.edges.forEach(element => {
    if (element.node.type !== 'job') return
    components.push(<SingleJob key={element.node.id} props={element.node} />);
  })
  return components;
}

function getJobText(data) {
  const components = []
  data.allDataJson.edges.forEach(element => {
    if (element.node.type !== 'jobText') return
    components.push(<p>{element.node.description}</p>);
  })
  return components;
}

const Jobs = () => (
  <StaticQuery
    query={graphql`
      query JobsQuery {
        allDataJson {
          edges {
            node {
              id
              title
              place
              description
              responsibilities
              requirements
              type
            }
          }
        }
      }
    `}
    render={data => (
      <Wrapper>
        {/* <Text>oferta</Text> */}
        {/* {getJobText(data)} */}
        <JobsContainer>
          {getJobComponents(data)}
        </JobsContainer>
        <MailText>
          Zainteresowane osoby zapraszamy do wysyłania CV na adres:
        </MailText>
        <Mail href='mailto:praca@molkom.pl'>praca@molkom.pl</Mail>
        <MailText>
        Zastrzegamy sobie prawo do odpowiedzi na wybrane oferty.
        </MailText>
        <MailPostfix>
        Prosimy o dopisanie następującej klauzuli: Wyrażam zgodę na przetwarzanie moich danych osobowych zawartych w mojej ofercie pracy dla potrzeb niezbędnych do realizacji procesu rekrutacji (zgodnie z Ustawą z dn. 29.08.97 o Ochronie danych Osobowych Dz. U. nr 133 poz. 883 z póżn. zm.)
        </MailPostfix>
      </Wrapper>
    )}
  />
)



export default Jobs
