export const meta = [
    {
        name: 'apple-mobile-web-app-title',
        content: 'MOLKOM',
    },
    {
        property: 'og:type',
        content: 'website',
    },
    {
        property: 'og:url',
        content: 'molkom.pl',
    },
    {
        property: 'og:title',
        content: 'MOLKOM | Profesjonalne rozwiązania informatyczne'
    },
    {
        property: 'og:image',
        content:'imagelink',
    },
    {
        property: 'og:description',
        content: 'MOLKOM | Profesjonalne rozwiązania informatyczne',
    },
    { 
        property: 'og:site_name',
        content: 'MOLKOM' },
    { 
        property: 'og:locale',
        content: 'pl_PL' 
    },
]