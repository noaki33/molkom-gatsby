import styled from 'styled-components'
import { Media } from './media'

const Container = styled.div`
  @media screen and (min-width: ${Media.tablet}) {
    width: 750px;
    display: flex;
  }
  @media screen and (min-width: ${Media.laptop}) {
    width: 970px;
  }
  @media screen and (min-width: ${Media.desktop}) {
    width: 1170px;
  }
`
export default Container