export const Colors = {
    mainColor: '#61408e',
    secondaryColor: '#e4e4e4',
    accentColor: '#33d457',
}