import React from 'react'
import styled from 'styled-components'

import { Colors } from '../colors'
import { Media } from '../media'

import DetailsComponent from './details-component'

const Wrapper = styled.div`
height: auto;
  @media screen and (min-width: ${Media.mobile}) {
    height: 750px;
  }
`

const MainContainer = styled.div`
  width: 90%;
  height: 20rem;
  border-radius: 10px;
  box-shadow: 0 10px 10px rgba(174, 167, 159, 0.3);
  margin: 2rem auto 0 auto;
  display: flex;
  flex-direction: column;
  background-color: ${Colors.mainColor};
  color: #fff;
  @media screen and (min-width: ${Media.tablet}) {
    width: 70%;
  }
`

const Text = styled.div`
  width: 100%;
  text-align: center;
  line-height: 1.3rem;
`

const BottomContainer = styled.div`
  display: flex;
  flex-direction: column;
  @media screen and (min-width: ${Media.tablet}) {
    flex-direction: row;
  }
`

const Title = styled.h1`
  text-align: center;
  border: none;
`

export default () => (
  <Wrapper>
    <MainContainer>
      <Title>Dane firmy i kontakt</Title>
      <Text>
        MOLKOM <br />
        Paweł Bartczak <br />
        ul. Poznańska 46/1
        <br />
        62-800 Kalisz
        <br />
        NIP: 668-175-52-56 <br />
        e-mail: kontakt@molkom.pl <br />
      </Text>
      <BottomContainer>
        <DetailsComponent
          props={{
            subtitle: 'Usługi programistyczne, strony www',
            text1: 'Paweł Bartczak',
            text2: 'tel. 692 931 521 ',
          }}
        />
        <DetailsComponent
          props={{
            subtitle: 'Obsługa informatyczna',
            text1: 'Michał Kujawa',
            text2: 'tel. 604 629 583',
          }}
        />
      </BottomContainer>
    </MainContainer>
  </Wrapper>
)
