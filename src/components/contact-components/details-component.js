import React from 'react'
import styled from 'styled-components'

import { Colors } from '../colors'
import { Media } from '../media'

const Wrapper = styled.div`
  min-height: 12rem;
  width: 80%;
  border-radius: 10px;
  margin: 3rem auto 0;
  background-color: #fff;
  box-shadow: 0 10px 10px rgba(174, 167, 159, 0.3);
  color: ${Colors.mainColor};

  @media screen and (min-width: ${Media.tablet}) {
    flex-direction: row;
    margin: 3rem 1rem 0;
    width: 60%;
  }
`
const Subtitle = styled.h2`
  text-align: center;
  padding: 0.3rem;
  margin-bottom: 0.7rem;
`
const Text = styled.div`
  width: 100%;
  text-align: center;
  line-height: 1.3rem;
`
export default ({ props }) => (
  <Wrapper>
    <Subtitle>{props.subtitle}</Subtitle>
    <Text>{props.text1}</Text>
    <Text>{props.text2}</Text>
  </Wrapper>
)
