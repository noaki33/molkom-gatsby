import React from 'react'
import styled from 'styled-components'

import Navigation from './navigation/navigation'

const Wrapper = styled.div`
`

export default () => {
  return (
    <Wrapper>
      <Navigation />
    </Wrapper>
  )
}
