import React from 'react'
import PropTypes from 'prop-types'
import { StaticQuery, graphql } from 'gatsby'
import Helmet from 'react-helmet'
import styled from 'styled-components'

import { meta } from './meta'
import Header from './header'
import Footer from  './footer'
import { Media } from './media'

const Wrapper = styled.div`
  margin: 0 auto;
  padding-top: 0;
  min-height: calc(100vh - 60px - 80px);
  @media only screen and (max_width: ${Media.tablet}) {
    overflow: hidden;
    height: 100%;
  }
`

const Layout = ({ children }) => (
  <StaticQuery
    query={graphql`
      query SiteTitleQuery {
        site {
          siteMetadata {
            title
          }
        }
      }
    `}
    render={data => (
      <>
        <Helmet
          meta={[
            {
              name: 'description',
              content: data.site.siteMetadata.title,
            },
            ...meta,
          ]}
        />
        <Header siteTitle={data.site.siteMetadata.title} />
        <Wrapper>
          {children}
        </Wrapper>
        <Footer/>
      </>
    )}
  />
)

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
