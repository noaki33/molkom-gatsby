import React from 'react'
import styled from 'styled-components'

import { Colors } from '../colors'
import Container from '../container'
import { Media } from '../media'

const Wrapper = styled.div`
  min-height: 400px;
  background-color: ${props =>
    props.odd ? `${Colors.mainColor}` : `${Colors.secondaryColor}`};
  padding: 30px 0;
`

const Content = styled(Container)`
  display: flex;
  flex-direction: column;
  margin: 0 auto;
  min-height: 250px;
`

const TextContainer = styled.div`
  width: 100%;
  padding: 0 1.3rem;

  @media screen and (min-width: ${Media.tablet}) {
    width: 66%;
  }
`

const ImageContainer = styled.div`
  width: 33%;
  background: url(${props=>props.image});
  background-repeat: no-repeat;
  background-size: contain;
  min-height: 250px;
  margin-top: -30px;
  display:none;
  @media screen and (min-width: ${Media.tablet}) {
    display: block;
  }
`

const Title = styled.h2`
  color: ${props => (props.odd ? '#fff' : '#000')};
  text-align: center;
  padding: 0 1.3rem;

  @media screen and (min-width: ${Media.tablet}) {
    text-align: ${props => (props.odd ? 'right' : 'left')};
  }
`

const TextImageContainer = styled.div`
  display: flex;
  flex-direction: row;
  flex-direction: ${props => (props.odd ? `row-reverse` : `row`)};
  text-align: center;
  color: ${props => (props.odd ? '#fff' : '#000')};
  height: 100%;

  @media screen and (min-width: ${Media.tablet}) {
    text-align: ${props => (props.odd ? 'right' : 'left')};
  }
`

export default ({ props }) => (
  <Wrapper odd={props.odd}>
    <Content>
      <Title odd={props.odd}>{props.title}</Title>
      <TextImageContainer odd={props.odd}>
        <TextContainer odd={props.odd}>
          {props.description}
        </TextContainer>
        <ImageContainer image={props.image}/>
      </TextImageContainer>
    </Content>
  </Wrapper>
)
