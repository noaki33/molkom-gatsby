import React from 'react'
import styled from 'styled-components'

import { Colors } from './colors'
import { Media } from './media'

import Container from './container'

const Background = styled.div`
  background-color: ${Colors.secondaryColor};
  box-sizing: border-box;
  width: 100%;
`

const Wrapper = styled(Container)`
  height: 260px;
  margin: 0 auto;
  padding-top: 20px;
  display: flex;
  flex-direction: column;
  @media screen and (min-width: ${Media.tablet}) {
    height: 200px;
  }
`

const Section = styled.div`
  width: 33%;
  text-align:center;
  line-height: 1.3rem;
`

const FooterHeader = styled.h4`
  color: ${Colors.mainColor};
  text-transform: uppercase;
`
const SectionWrapper = styled.div`
  display: flex;
  flex-direction: row;
  margin: 0 auto;
  width: 100%;
  justify-content: center;
`

export default () => (
  <Background>
    <Wrapper>
      <SectionWrapper>
        <Section>
          <FooterHeader>molkom</FooterHeader>
          Paweł Bartczak <br />
          ul. Poznańska 46/1 <br />
          62-800 Kalisz <br />
          NIP: 668-175-52-56 <br />
          e-mail: kontakt@molkom.pl <br />
        </Section>
        <Section>
          <FooterHeader>
            uslugi programistyczne, <br /> aplikacje webowe
          </FooterHeader>
          Paweł Bartczak <br />
          tel. 692 951 521
        </Section>
        <Section>
        <FooterHeader>obsluga informatyczna</FooterHeader>
          Michał Kujawa <br />
          tel. 604 629 583
        </Section>
      </SectionWrapper>
    </Wrapper>
  </Background>
)
